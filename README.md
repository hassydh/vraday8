# VRA day8


## 使用機器
* Oculus Quest（ファームのバージョン　8.0.0.135）

## Unityバージョン
* 2018.4.10.f1  
2018.4系なら問題なく動くと思います  
Oculus Integrationは1.40で18.4系を正式にサポートしました  
LTSですし18.4系がおすすめです。

## 使用ライブラリ
* Oculus Integration v1.42  
（ただし一部だけを残して削除しています）

## ビルド時にエラーが出る場合
Oculusフォルダを削除し（Oculus Integrationがはいっているフォルダ）、アセットストアから、Oculus Ingegrationをダウンロードして取り込みなおしてみてください。
