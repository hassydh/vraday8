﻿using UnityEngine;

public class GeneratorScript : MonoBehaviour {

    public GameObject leftProjectilePfb;//左手用に設定したプレファブ
    public GameObject rightProjectilePfb;//右手用に設定したプレファブ

    float timer;
    public float intervalSec = 3.0f;//何秒感覚でプレファブを生成するか設定できます

	// Update is called once per frame
	void Update () {
        timer = timer + Time.deltaTime;//アップデートのたびに呼び出された時間を足し算
        if(timer > intervalSec)
        {//合計がインターバルより大きくなったら実行
            timer = 0;//タイマーをリセットしておく
            GenerateProjectile();
        }
	}

    void GenerateProjectile()
    {
        //ランダムで、1/2の確率で、右手できるオブジェクトか左手できるオブジェクトかをきりかえる
        GameObject targetPrefab;
        int randomSw = Random.Range(1, 3);
        if(randomSw == 1)
        {
            targetPrefab = leftProjectilePfb;
        }
        else
        {
            targetPrefab = rightProjectilePfb;
        }

        //飛び出す方向は　Target　というタグが付いているオブジェクト
        GameObject[] targetObjs = GameObject.FindGameObjectsWithTag("Target");
        //複数あるターゲットのうちランダムで向かう先を決める
        int randomTarget = Random.Range(0, targetObjs.Length);
        //Instantiateでプレファブをシーンに生成してやる
        GameObject tempObj = Instantiate(targetPrefab, transform.position, Quaternion.identity);
        //targetを向くように設定
        tempObj.transform.LookAt(targetObjs[randomTarget].transform);

        ////自分で計算して向きをもとめるならこんなやり方もあります
        ////自分自身の位置からターゲットの位置を引くことで、その方向のベクトルをつくって
        //Vector3 targetDir = targetObjs[randomTarget].transform.position - transform.position;
        ////ベクトルから向き（Quarternion）に変換
        //Quaternion targetQ = Quaternion.LookRotation(targetDir);
        ////その向きにむけてInstantiateしてやれば、あとはその方向に直進していく
        //Instantiate(targetPrefab, transform.position, targetQ);
    }
}
