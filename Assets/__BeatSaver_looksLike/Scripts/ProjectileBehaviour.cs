﻿using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour {

    //publicになっているものはインスペクターで調整できます
    public float speed = 5f;//飛んでいく速度
    public bool isRightMode = false;//右手キューブか左手キューブか
    public float lifetime = 5.0f;//何秒でシーンから消えるか

	// Use this for initialization
	void Start () {
        //一定時間たつと自動で切りそこねてもオブジェクトが消えるように
        Destroy(this.gameObject, lifetime);

        //右手用か左手用かで色をかえる、小要素のもっているRendererの色を変えてやる
        Renderer re = GetComponentInChildren<Renderer>();
        if(re  != null)
        {
            if(isRightMode)
            {
                re.material.color = Color.blue;
            }
            else
            {
                re.material.color = Color.red;
            }
        }
	}
    
    // Update is called once per frame
	void Update () {
        //Z軸　＝　自分の正面方向に足し算　＝　前に向かって真っ直ぐ進む
        transform.Translate(0, 0, speed * Time.deltaTime);
	}
}