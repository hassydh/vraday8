﻿using UnityEngine;

public class SwordBehaviour : MonoBehaviour
{
    AudioSource asourse;
    public AudioClip trueClip;//正しいオブジェクトを切ったときの音
    public AudioClip falseClip;//オブジェクトをまちがって切ったときの音
    public bool isMySwordRight = false;

    // Use this for initialization
    void Start()
    {
        asourse = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Projectile"))//接触した相手のタグがProjectileのときだけ実行
        {
            //ぶつかったProjectileが右手の設定かどうか
            bool isObjRight = other.gameObject.GetComponent<ProjectileBehaviour>().isRightMode;

            if (isObjRight == isMySwordRight)//ぶつかった相手の右左設定と、自分の右左設定が、同じなら正解と判断
            {
                asourse.PlayOneShot(trueClip);//正解の音を出す
            }
            else
            {
                asourse.PlayOneShot(falseClip);//不正解の音をだす
            }
            //ぶつかったあいてのオブジェクトを消す
            Destroy(other.gameObject);
        }
    }
}
